package jpmc.demo.com.demojpmc.album;

import java.util.ArrayList;
import java.util.List;

import jpmc.demo.com.demojpmc.model.Album;

/**
 * Created by sumerchawla on 9/30/18.
 */


public interface AlbumContract {


    interface presenter{

        void onDestroy();


        void requestDataFromServer();

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataToRecyclerView(List<Album> albumArrayList);

        void onResponseFailure(Throwable throwable);


    }


    interface GetAlbumsIntractor {

        interface OnFinishedListener {
            void onFinished(List<Album> albumArrayList);
            void onFailure(Throwable t);
        }

        void getAlbumsList(OnFinishedListener onFinishedListener);
    }
}