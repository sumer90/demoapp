package jpmc.demo.com.demojpmc.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import jpmc.demo.com.demojpmc.R;
import jpmc.demo.com.demojpmc.model.Album;

/**
 * Created by sumerchawla on 10/1/18.
 */

public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.AlbumViewHolder> {

    private List<Album> dataList;
    private RecyclerItemClickListener recyclerItemClickListener;

    public AlbumsAdapter(List<Album> dataList , RecyclerItemClickListener recyclerItemClickListener) {
        this.dataList = dataList;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.ablum_row_item, parent, false);
        return new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AlbumViewHolder holder, final  int position) {
        holder.txtAlbumTitle.setText(dataList.get(position).getTitle());
        holder.txtAlbumId.setText(""+dataList.get(position).getId());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerItemClickListener.onItemClick(dataList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class AlbumViewHolder extends RecyclerView.ViewHolder {

        TextView txtAlbumTitle, txtAlbumId;

        AlbumViewHolder(View itemView) {
            super(itemView);
            txtAlbumTitle =  itemView.findViewById(R.id.txt_album_title);
            txtAlbumId =  itemView.findViewById(R.id.txt_album_id);

        }
    }
    public interface RecyclerItemClickListener {
        void onItemClick(Album notice);
    }

}