package jpmc.demo.com.demojpmc.retrofit;

import java.util.List;

import jpmc.demo.com.demojpmc.model.Album;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by sumerchawla on 10/1/18.
 */

public interface GetDataFromServer {

    @GET("albums/")
    Call<List<Album>> getAlbumsData();


}
