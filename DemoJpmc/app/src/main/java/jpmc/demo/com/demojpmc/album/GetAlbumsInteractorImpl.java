package jpmc.demo.com.demojpmc.album;

import android.util.Log;

import java.util.List;

import jpmc.demo.com.demojpmc.model.Album;
import jpmc.demo.com.demojpmc.retrofit.GetDataFromServer;
import jpmc.demo.com.demojpmc.retrofit.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sumerchawla on 9/30/18.
 */

public class GetAlbumsInteractorImpl implements AlbumContract.GetAlbumsIntractor {
    @Override
    public void getAlbumsList(final OnFinishedListener onFinishedListener) {

        GetDataFromServer service = RetrofitInstance.getRetrofitInstance().create(GetDataFromServer.class);

        /** Call the method with parameter in the interface to get the albums data*/
        Call<List<Album>> call = service.getAlbumsData();

        /**Log the URL called*/
        Log.d("URL Called", call.request().url() + "");

        call.enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                onFinishedListener.onFinished(response.body());

            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
                onFinishedListener.onFailure(t);

            }


        });
    }
}
