package jpmc.demo.com.demojpmc.album;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import jpmc.demo.com.demojpmc.R;
import jpmc.demo.com.demojpmc.adapter.AlbumsAdapter;
import jpmc.demo.com.demojpmc.model.Album;
import jpmc.demo.com.demojpmc.utils.BaseActivity;

/**
 * Created by sumerchawla on 9/30/18.
 */

public class AlbumActivity extends BaseActivity implements AlbumContract.MainView{

    private ProgressBar progressBar;
    private RecyclerView recyclerView;

    private AlbumContract.presenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_albums);
        initializeToolbarAndRecyclerView();
        initProgressBar();


        presenter = new AlbumsPresenterImpl(this, new GetAlbumsInteractorImpl());
        presenter.requestDataFromServer();
    }

    private void initializeToolbarAndRecyclerView() {


        recyclerView = findViewById(R.id.recycler_view_album_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(AlbumActivity.this);
        recyclerView.setLayoutManager(layoutManager);


    }



    private void initProgressBar() {
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(progressBar);

        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progressBar.setVisibility(View.INVISIBLE);

        this.addContentView(relativeLayout, params);
    }

    private AlbumsAdapter.RecyclerItemClickListener recyclerItemClickListener = new AlbumsAdapter.RecyclerItemClickListener() {
        @Override
        public void onItemClick(Album album) {

            Toast.makeText(AlbumActivity.this,
                    "List title:  " + album.getTitle(),
                    Toast.LENGTH_LONG).show();

        }
    };


    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setDataToRecyclerView(List<Album> albumArrayList) {
        AlbumsAdapter adapter = new AlbumsAdapter(albumArrayList , recyclerItemClickListener);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
       /* Toast.makeText(AlbumActivity.this,
                "Something went wrong...Error message: " + throwable.getMessage(),
                Toast.LENGTH_LONG).show();*/

        showErrorPopup(this,throwable.getMessage());

    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

}
