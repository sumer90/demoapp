package jpmc.demo.com.demojpmc.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import jpmc.demo.com.demojpmc.R;

/**
 * Created by geetikasuri on 10/1/18.
 */

public class BaseActivity extends AppCompatActivity {

    public void showErrorPopup(Context context,String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
finish();            }
        });
        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
