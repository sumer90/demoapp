package jpmc.demo.com.demojpmc.album;

import java.util.ArrayList;
import java.util.List;

import jpmc.demo.com.demojpmc.model.Album;

/**
 * Created by sumerchawla on 10/1/18.
 */

public class AlbumsPresenterImpl implements AlbumContract.presenter, AlbumContract.GetAlbumsIntractor.OnFinishedListener {


    private AlbumContract.MainView mainView;
    private AlbumContract.GetAlbumsIntractor getAlbumsIntractor;

    public AlbumsPresenterImpl(AlbumContract.MainView mainView, AlbumContract.GetAlbumsIntractor getAlbumsIntractor) {
        this.mainView = mainView;
        this.getAlbumsIntractor = getAlbumsIntractor;
    }

    @Override
    public void onDestroy() {
        mainView = null;

    }



    @Override
    public void requestDataFromServer() {
        if(mainView != null)
            mainView.showProgress();
        getAlbumsIntractor.getAlbumsList(this);

    }

    @Override
    public void onFinished(List<Album> albumArrayList) {
        if(mainView != null){
            mainView.setDataToRecyclerView(albumArrayList);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(mainView != null){
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
